package cmd

import (
	"github.com/spf13/cobra"
)

var getFirmwareInfoCmd = &cobra.Command{
	Use:   "getFirmwareInfo",
	Short: "get firmware info",
	Long:  `get firmware info`,
	RunE: func(cmd *cobra.Command, args []string) error {
		_, err := client.InvokeGcodeScript(`M115`)

		return err
	},
}

func init() {
	rootCmd.AddCommand(getFirmwareInfoCmd)
}
