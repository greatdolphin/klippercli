package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

// getFieldValueCmd represents the getFieldValue command
var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "move x or/and y or/and z",
	Long:  `move x or/and y or/and z in mm or inch units absolutely or relatively`,
	RunE: func(cmd *cobra.Command, args []string) error {
		script := fmt.Sprintf(`G0 X%f Y%f Z%f`, *x, *y, *z)
		log.Println(`send script:`, script)

		_, err := client.InvokeGcodeScript(script)

		return err
	},
}

var (
	x, y, z *float32
)

func init() {
	rootCmd.AddCommand(moveCmd)
	x = moveCmd.PersistentFlags().Float32(`x`, 0.0, `x axis`)
	y = moveCmd.PersistentFlags().Float32(`y`, 0.0, `y axis`)
	z = moveCmd.PersistentFlags().Float32(`z`, 0.0, `z axis`)
}
