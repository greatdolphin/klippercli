package cmd

import (
	"context"

	"log"

	"github.com/spf13/cobra"
	"gitlab.com/greatdolphin/klippercli/pkg/udsclient"
)

/*
sub commands:
	"getFirmwareInfo",  //M115
	"setUnitInch",  //G20
	"setUnitMM",  //G21
	"goHome",  //G28
	"setPosAbsolute", //G90
	"setPosRelative", //G91
	"move", //G0; non-extrusion movements
	"getPos",  //M114 R; need M114_REALTIME option
*/

// rootCmd represents the base command when called without any subcommands
var (
	rootCmd = &cobra.Command{
		Use:   "klippercli",
		Short: "klippercli",
		Long:  `simsctl is a cli tool to access klipper by its api socket`,
	}

	ctx    = context.Background()
	client udsclient.IUDSClient
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	var err error
	client, err = udsclient.NewUDSClient(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	cobra.CheckErr(rootCmd.Execute())
}
