package main

import (
	"gitlab.com/greatdolphin/klippercli/bin/klippercli/cmd"
)

func main() {
	cmd.Execute()
}
