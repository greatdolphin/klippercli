module gitlab.com/greatdolphin/klippercli

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/spf13/cobra v1.5.0
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
