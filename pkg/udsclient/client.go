package udsclient

import (
	"context"
	"log"
	"net"
	"time"
)

type JsonObj string

const (
	DELIMITER    = byte(0x30)
	BUFSIZE      = 10240
	GCODETIMEOUT = 5
)

type IUDSClient interface {
	Write(JsonObj) error
	Read() error
	// Invoke gives a request and get a response with a timeout seconds
	Invoke(id string, jo JsonObj, timeout int) (JsonObj, error)
	InvokeGcodeScript(script string) (JsonObj, error)
}

type UDSClient struct {
	//sockFd      int
	conn        *net.UnixConn
	buffer      []byte
	bufPos      int
	responseMap map[string]JsonObj // key is message id

	ctx context.Context
}

func NewUDSClient(ctx context.Context) (IUDSClient, error) {

	unixAddress, err := net.ResolveUnixAddr("unix", "/tmp/klippy_uds")
	if err != nil {
		return nil, err
	}

	conn, err := net.DialUnix("unix", nil, unixAddress)
	if err != nil {
		return nil, err
	}

	//sa := &unix.SockaddrUnix{
	//	Name: "/tmp/klippy_uds",
	//}

	//fd, err := unix.Socket(unix.AF_UNIX, unix.SOCK_STREAM, 0)
	//if err != nil {
	//	return nil, err
	//}

	//if err := unix.Connect(fd, sa); err != nil {
	//	return nil, err
	//}

	client := &UDSClient{
		//sockFd:      fd,
		conn:        conn,
		buffer:      make([]byte, BUFSIZE),
		bufPos:      0,
		responseMap: make(map[string]JsonObj),
		ctx:         ctx,
	}

	go client.Read()

	return client, nil
}

func (c *UDSClient) Write(jo JsonObj) error {
	bs := append([]byte(jo), DELIMITER)

	_, err := c.conn.Write(bs)
	if err != nil {
		return err
	}
	//err := unix.Send(c.sockFd, bs, 0)

	return err
}

func (c *UDSClient) Read() error {
	for {
		//n, _, err := unix.Recvfrom(c.sockFd, c.buffer[c.bufPos:], 0)
		n, err := c.conn.Read(c.buffer[c.bufPos:])
		if err != nil {
			return err
		}
		log.Printf("Client got n:%d, resp:[%s]\n", n, string(c.buffer[c.bufPos:c.bufPos+n]))
	}
}

func (c *UDSClient) Invoke(id string, jo JsonObj, timeout int) (JsonObj, error) {
	if err := c.Write(jo); err != nil {
		return "", err
	}

	time.Sleep(3 * time.Second)

	return "", nil
}

func (c *UDSClient) InvokeGcodeScript(script string) (JsonObj, error) {
	id, jo := JsonObjOfGcodeScript(script)

	return c.Invoke(id, jo, GCODETIMEOUT)
}
