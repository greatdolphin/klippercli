package udsclient

import (
	"fmt"

	"github.com/google/uuid"
)

func JsonObjOfGcodeScript(script string) (id string, jo JsonObj)  {
	id = uuid.New().String()
	jo = JsonObj(fmt.Sprintf(
		`{"id": "%s", "method": "gcode/script", "params": {"script": "%s"}}`, id, script))
	return
}
